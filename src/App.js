import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';

import {Container} from 'react-bootstrap';

import './App.css';

function App() {
  return (
    // Common pattern in react.js for a component to return multiple elements.
    <>
    <AppNavbar />
    <Container>
      <Home />
      <Courses />
    </Container>
    </>
  );
}

export default App;
